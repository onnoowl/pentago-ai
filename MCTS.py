import math
import numpy as np
from pentago.PentagoGame import PentagoGame
EPS = 1e-8

class MCTS():
    """
    This class handles the MCTS tree.
    """

    def __init__(self, game, nnet, args):
        self.game = game
        self.nnet = nnet
        self.args = args
        self.Qsa = {}       # stores Q values for s,a (as defined in the paper)
        self.Nsa = {}       # stores #times edge s,a was visited
        self.Ns = {}        # stores #times board s was visited
        self.Ps = {}        # stores initial policy (returned by neural net)

        self.Es = {}        # stores game.getGameEnded ended for board s
        self.Vs = {}        # stores game.getValidMoves for board s

        self.simplePolicyExplored = set()

    def getBestU(self, s):
        cur_best = -float('inf')
        valids = self.Vs[s]
        # pick the action with the highest upper confidence bound
        for a in range(self.game.getActionSize()):
            if valids[a]:
                if (s,a) in self.Qsa:
                    u = self.Qsa[(s,a)] + self.args["cpuct"]*self.Ps[s][a]*math.sqrt(self.Ns[s])/(1+self.Nsa[(s,a)])
                else:
                    u = self.args["cpuct"]*self.Ps[s][a]*math.sqrt(self.Ns[s] + EPS)     # Q = 0 ?

                if u > cur_best:
                    cur_best = u

        return cur_best

    def getActionProb(self, canonicalBoard, temp=1, useAlt=False):
        """
        This function performs numMCTSSims simulations of MCTS starting from
        canonicalBoard.

        Returns:
            probs: a policy vector where the probability of the ith action is
                   proportional to Nsa[(s,a)]**(1./temp)
        """
        self.simplePolicyExplored = set()
        instaWinWhitelist = np.ones((self.game.getActionSize(),))
        tiesWhitelist = np.ones((self.game.getActionSize(),))

        s = self.game.stringRepresentation(canonicalBoard)

        numMCTS = self.args["numMCTSSims"]
        if useAlt:
            numMCTS = self.args["altNumMCTSSims"]


        searchesPerformed = 0
        printedOnce = False
        while searchesPerformed < self.args["maxNumMCTSSims"]:
            for i in range(numMCTS):
                self.search(canonicalBoard, 0)
                searchesPerformed += 1

            counts = np.array([self.Nsa[(s,a)] if (s,a) in self.Nsa else 0 for a in range(self.game.getActionSize())])

            if temp==0: # TODO: Add into normal tree search somehow?
                bestA = np.argmax(counts*instaWinWhitelist*tiesWhitelist)
                topLevelValids = self.Vs[s]
                if not topLevelValids[bestA]:
                    break
                next_s, next_player = self.game.getNextState(canonicalBoard, 1, bestA)
                next_s = self.game.getCanonicalForm(next_s, next_player)
                next_s_str = self.game.stringRepresentation(next_s)

                if self.game.getGameEnded(next_s, 1) != -1: #if bestA is a winning move, we don't need to consider retries
                    if next_s_str in self.Vs:
                        valids = self.Vs[next_s_str]
                    else:
                        valids = self.game.getValidMoves(next_s, 1)
                        self.Vs[next_s_str] = valids

                    # pick the action with the highest upper confidence bound
                    needToRetry = False
                    for a in range(self.game.getActionSize()):
                        if valids[a]:
                            next_next_s, next_next_player = self.game.getNextState(next_s, 1, a)
                            outcome = self.game.getGameEnded(next_next_s, 1)
                            if outcome == 1: # OPPONENT WINS
                                opponentCanWinInOne = True
                                instaWinWhitelist[bestA] = 0
                                self.Qsa[s, bestA] = -1
                            if outcome != 0 and outcome != -1: # TIE
                                needToRetry = True
                                tiesWhitelist[bestA] = 0
                                self.Qsa[s, bestA] = -outcome
                                break
                    if needToRetry:
                        if not printedOnce:
                            print("Hm... I need to think some more...")
                            printedOnce = True
                        # PentagoGame.display(next_s)
                        numMCTS = self.args['retryNumMCTSSims']
                        continue

            break



        topLevelActionsExplored = 0
        topLevelValidActions = 0
        topLevelValids = self.Vs[s]
        for a in range(self.game.getActionSize()):
            if (s,a) in self.Qsa:
                topLevelActionsExplored += 1
            if topLevelValids[a]:
                topLevelValidActions += 1
        print(topLevelActionsExplored, "/", topLevelValidActions, "actions explored.")

        if temp==0:
            bestA = np.argmax(counts*instaWinWhitelist*tiesWhitelist) # consider moves that prevent wins and ties
            if not topLevelValids[bestA]:
                print("I think I'm in trouble...")
                bestA = np.argmax(counts*instaWinWhitelist) # next consider moves that prevent wins (but allow ties)
                if not topLevelValids[bestA]:
                    print("Serious trouble...")
                    bestA = np.argmax(counts) # consider any move... this means we're going ot lose or tie
            probs = [0]*len(counts)
            probs[bestA]=1
            return probs

        counts = [x**(1./temp) for x in counts]
        counts_sum = float(sum(counts))
        probs = [x/counts_sum for x in counts]
        return probs


    def search(self, canonicalBoard, depth):
        """
        This function performs one iteration of MCTS. It is recursively called
        till a leaf node is found. The action chosen at each node is one that
        has the maximum upper confidence bound as in the paper.

        Once a leaf node is found, the neural network is called to return an
        initial policy P and a value v for the state. This value is propagated
        up the search path. In case the leaf node is a terminal state, the
        outcome is propagated up the search path. The values of Ns, Nsa, Qsa are
        updated.

        NOTE: the return values are the negative of the value of the current
        state. This is done since v is in [-1,1] and if v is the value of a
        state for the current player, then its value is -v for the other player.

        Returns:
            v: the negative of the value of the current canonicalBoard
        """
        s = self.game.stringRepresentation(canonicalBoard)

        if s not in self.Es:
            self.Es[s] = self.game.getGameEnded(canonicalBoard, 1)
        if self.Es[s]!=0:
            # terminal node
            # print("d" + str(depth), end=" ")
            return -self.Es[s]

        if s not in self.Ps:
            # print(depth, end=" ")
            # leaf node
            self.Ps[s], v = self.nnet.predict(canonicalBoard)
            valids = self.game.getValidMoves(canonicalBoard, 1)
            self.Ps[s] = self.Ps[s]*valids      # masking invalid moves
            sum_Ps_s = np.sum(self.Ps[s])
            if sum_Ps_s > 0:
                self.Ps[s] /= sum_Ps_s    # renormalize
            else:
                # if all valid moves were masked make all valid moves equally probable

                # NB! All valid moves may be masked if either your NNet architecture is insufficient or you've get overfitting or something else.
                # If you have got dozens or hundreds of these messages you should pay attention to your NNet and/or training process.
                print("All valid moves were masked, do workaround.")
                self.Ps[s] = self.Ps[s] + valids
                self.Ps[s] /= np.sum(self.Ps[s])

            self.Vs[s] = valids
            self.Ns[s] = 0
            return -v

        valids = self.Vs[s]

        # We always want to consider the best action from the simple policy for depths 0 and 1 of the search tree.
        # This makes sure we still explore simple 1-away-from-winning moves while the neural net is still sub-par.
        if 'simplePolicy' in self.args and self.args["simplePolicy"] != None and depth in [0, 1] and s not in self.simplePolicyExplored:
            self.simplePolicyExplored.add(s)
            a = np.argmax(self.args["simplePolicy"](self.game, canonicalBoard))
        else:
            cur_best = -float('inf')
            best_act = -1
            # pick the action with the highest upper confidence bound
            for a in range(self.game.getActionSize()):
                if valids[a]:
                    if (s,a) in self.Qsa:
                        u = self.Qsa[(s,a)] + self.args["cpuct"]*self.Ps[s][a]*math.sqrt(self.Ns[s])/(1+self.Nsa[(s,a)])
                    else:
                        u = self.args["cpuct"]*self.Ps[s][a]*math.sqrt(self.Ns[s] + EPS)     # Q = 0 ?

                    if u > cur_best:
                        cur_best = u
                        best_act = a

            a = best_act

        next_s, next_player = self.game.getNextState(canonicalBoard, 1, a)
        next_s = self.game.getCanonicalForm(next_s, next_player)

        v = self.search(next_s, depth + 1)

        if (s,a) in self.Qsa:
            self.Qsa[(s,a)] = (self.Nsa[(s,a)]*self.Qsa[(s,a)] + v)/(self.Nsa[(s,a)]+1)
            self.Nsa[(s,a)] += 1

        else:
            self.Qsa[(s,a)] = v
            self.Nsa[(s,a)] = 1

        self.Ns[s] += 1
        return -v
