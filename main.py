from Coach import Coach
from pentago.PentagoGame import PentagoGame as Game
from pentago.tensorflow.NNet import NNetWrapper as nn
from pentago.PentagoPlayers import greedyPolicyMaker
from utils import *

args = {
    'numIters': 1000,
    'numEps': 120,               # Number of complete self-play games to simulate during a new iteration.
    'tempThreshold': 15,        #
    'updateThreshold': 0.51,    # During arena playoff, new neural net will be accepted if threshold or more of games are won.
    'maxlenOfQueue': 200000,    # Number of game examples to train the neural networks.
    'numMCTSSims': 1000,          # Number of games moves for MCTS to simulate.
    'maxNumMCTSSims': 1000,
    'retryNumMCTSSims': 250,
    'altNumMCTSSims': 1000,          # Number of games moves for MCTS to simulate.
    'arenaCompare': 30,         # Number of games to play during arena play to determine if new net will be accepted.
    'cpuct': 1,

    'checkpoint': './tmp/',
    'load_model': True,
    'load_folder_file': ('./pretrained_pentago/','best.pth.tar'),
    'load_examples_file': ('./tmp/','checkpoint_0.pth.tar.examples'),
    'numItersForTrainExamplesHistory': 20,

    'simplePolicyMaxIter': 1,
    'simplePolicyArenaCompare': 20,
    'simplePolicy': greedyPolicyMaker,
    'simplePolicyStartingNumEps': 60,

    'startingNumEps': 120,               # Number of complete self-play games to simulate on startup.
    # 'pruneExamplesOnLoadTo': 5,

    'poolSize': 4
}

if __name__ == "__main__":
    import multiprocessing
    multiprocessing.set_start_method('spawn')

    g = Game()
    nnet = nn(g)

    if args["load_model"]:
        print("\nLoading Model")
        nnet.load_checkpoint(args["load_folder_file"][0], args["load_folder_file"][1])

    c = Coach(g, nnet, args)
    if args["load_model"]:
        print("Loading trainExamples from file")
        c.loadTrainExamples()
    c.learn()
