from collections import deque
from Arena import Arena
from MCTS import MCTS
import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time, os, sys
from pickle import Pickler, Unpickler
from random import shuffle

import multiprocessing
from multiprocessing import Pool, Queue, Process
from tqdm import tqdm

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' #only errors
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
# import tensorflow.python.util.deprecation as deprecation
# deprecation._PRINT_DEPRECATION_WARNINGS = False #TODO: This is bad

from pentago.tensorflow.NNet import NNetWrapper as nn # TODO: wont work for non pentago now
class Worker(Process):
    def __init__(self, game, args, numEps, progress, output, pi1Fun, pi2Fun):
        super(Worker, self).__init__()
        self.game = game
        self.args = args
        self.numEps = numEps
        self.progress = progress
        self.output = output
        self.pi1Fun = pi1Fun
        self.pi2Fun = pi2Fun

    def run(self):
        workerOutput = []

        nnet = nn(self.game)
        nnet.load_checkpoint(folder=self.args["checkpoint"], filename='temp.pth.tar')

        for i in range(self.numEps // self.args.get("poolSize", 1)):
            mcts = MCTS(self.game, nnet, self.args)   # reset search tree

            if self.pi1Fun == None:
                pi1Fun = lambda game, canonicalBoard, temp: mcts.getActionProb(canonicalBoard, temp=temp)
            else:
                pi1Fun = self.pi1Fun

            if self.pi2Fun == None:
                pi2Fun = lambda game, canonicalBoard, temp: mcts.getActionProb(canonicalBoard, temp=temp)
            else:
                pi2Fun = self.pi2Fun

            workerOutput.append(self.executeEpisode(self.game, self.args, pi1Fun, pi2Fun))

            self.progress.put(1)

        self.output.put(workerOutput)

    def executeEpisode(self, game, args, pi1Fun, pi2Fun):
        """
        This function executes one episode of self-play, starting with player 1.
        As the game is played, each turn is added as a training example to
        trainExamples. The game is played till the game ends. After the game
        ends, the outcome of the game is used to assign values to each example
        in trainExamples.

        It uses a temp=1 if episodeStep < tempThreshold, and thereafter
        uses temp=0.

        Returns:
            trainExamples: a list of examples of the form (canonicalBoard,pi,v)
                            pi is the MCTS informed policy vector, v is +1 if
                            the player eventually won the game, else -1.
        """
        trainExamples = []
        board = game.getInitBoard()
        curPlayer = 1
        episodeStep = 0

        while True:
            episodeStep += 1
            canonicalBoard = game.getCanonicalForm(board, curPlayer)
            temp = int(episodeStep < args["tempThreshold"])

            # pi = self.mcts.getActionProb(canonicalBoard, temp=temp)
            if episodeStep % 2 == 1:
                pi = pi1Fun(game, canonicalBoard, temp)
            else:
                pi = pi2Fun(game, canonicalBoard, temp)

            sym = game.getSymmetries(canonicalBoard, pi)
            for b,p in sym:
                trainExamples.append([b, curPlayer, p, None])

            action = np.random.choice(len(pi), p=pi)
            board, curPlayer = game.getNextState(board, curPlayer, action)

            r = game.getGameEnded(board, curPlayer)

            if r!=0:
                return [(x[0],x[2],r*((-1)**(x[1]!=curPlayer))) for x in trainExamples]

class Coach():
    """
    This class executes the self-play + learning. It uses the functions defined
    in Game and NeuralNet. args are specified in main.py.
    """

    def __init__(self, game, nnet, args):
        self.game = game
        self.nnet = nnet
        self.pnet = self.nnet.__class__(self.game)  # the competitor network
        self.args = args
        self.mcts = MCTS(self.game, self.nnet, self.args)
        self.trainExamplesHistory = []    # history of examples from args["numItersForTrainExamplesHistory"] latest iterations
        self.skipFirstSelfPlay = False    # can be overriden in loadTrainExamples()        multiprocessing.set_start_method('spawn')        multiprocessing.set_start_method('spawn')

    def _repeatedPlay(self, title, numEps, pi1Fun=None, pi2Fun=None):
        iterationTrainExamples = deque([], maxlen=self.args["maxlenOfQueue"])

        processes = list()
        poolSize = self.args.get("poolSize", 1)

        numEps = (numEps // poolSize) * poolSize

        progress = Queue(numEps)
        output = Queue(poolSize)
        for i in range(0, poolSize):
            p=Worker(game=self.game, args=self.args, numEps=numEps, output=output, progress=progress, pi1Fun=pi1Fun, pi2Fun=pi2Fun)
            p.start()
            processes.append(p)

        for _ in tqdm(range(0, numEps), total=numEps, desc=title):
            progress.get()

        for i in range(poolSize):
            workerOutput = output.get()
            for e in workerOutput:
                iterationTrainExamples += e
        [proc.terminate() for proc in processes]

        # for eps in range(numEps):
            # bookkeeping + plot progress
            # eps_time.update(time.time() - end)
            # end = time.time()
            # bar.suffix  = '({eps}/{maxeps}) Eps Time: {et:.3f}s | Total: {total:} | ETA: {eta:}'.format(eps=eps+1, maxeps=numEps, et=eps_time.avg,
            #                                                                                             total=bar.elapsed_td, eta=bar.eta_td)
            # bar.next()
        # bar.finish()

        return iterationTrainExamples

    def learn(self):
        """
        Performs numIters iterations with numEps episodes of self-play in each
        iteration. After every iteration, it retrains neural network with
        examples in trainExamples (which has a maximum length of maxlenofQueue).
        It then pits the new neural network against the old one and accepts it
        only if it wins >= updateThreshold fraction of games.
        """

        self.nnet.save_checkpoint(folder=self.args["checkpoint"], filename='temp.pth.tar')

        if self.args["simplePolicy"] != None and self.args["simplePolicyMaxIter"] > 0:
            self.trainExamplesHistory.append(self._repeatedPlay('Simple Policy Self Play', self.args["simplePolicyStartingNumEps"], pi1Fun=self.args["simplePolicy"], pi2Fun=self.args["simplePolicy"]))

        for i in range(1, self.args["numIters"]+1):
            # bookkeeping
            print('------ITER ' + str(i) + '------')
            # examples of the iteration
            if not self.skipFirstSelfPlay or i > 1 or 'startingNumEps' in self.args:
                numEps = self.args["numEps"]
                if 'startingNumEps' in self.args:
                    numEps = self.args["startingNumEps"]
                if self.args["simplePolicy"] == None or (self.args["simplePolicyMaxIter"] != None and i > self.args["simplePolicyMaxIter"]):
                    self.trainExamplesHistory.append(self._repeatedPlay('Self Play', numEps))
                else:
                    self.trainExamplesHistory.append(self._repeatedPlay('Self Play', numEps//2))
                    self.trainExamplesHistory.append(self._repeatedPlay('Self vs Simple Policy Play', numEps//4, pi2Fun=self.args["simplePolicy"]))
                    self.trainExamplesHistory.append(self._repeatedPlay('Simple Policy Play vs Self', numEps//4, pi1Fun=self.args["simplePolicy"]))

            if len(self.trainExamplesHistory) > self.args["numItersForTrainExamplesHistory"]:
                print("len(trainExamplesHistory) =", len(self.trainExamplesHistory), " => remove the oldest trainExamples")
                self.trainExamplesHistory.pop(0)
            # backup history to a file
            # NB! the examples were collected using the model from the previous iteration, so (i-1)
            print("Saving train examples")
            self.saveTrainExamples(i-1)

            # shuffle examples before training
            trainExamples = []
            for e in self.trainExamplesHistory:
                trainExamples.extend(e)
            shuffle(trainExamples)

            # training new network, keeping a copy of the old one
            self.nnet.save_checkpoint(folder=self.args["checkpoint"], filename='temp.pth.tar')
            self.pnet.load_checkpoint(folder=self.args["checkpoint"], filename='temp.pth.tar')
            pmcts = MCTS(self.game, self.pnet, self.args)

            self.nnet.train(trainExamples)

            # if self.args["simplePolicy"] != None and self.args["simplePolicyArenaCompare"]:
            #     nmcts = MCTS(self.game, self.nnet, self.args)

            #     print('PITTING AGAINST SIMPLE POLICY')
            #     arena = Arena(lambda x: np.argmax(nmcts.getActionProb(x, temp=0, useAlt=True)),
            #                 lambda x: np.argmax(self.args["simplePolicy"](self.game, x)), self.game)
            #     nwins, swins, draws = arena.playGames(self.args["simplePolicyArenaCompare"])
            #     print('NEW/SIMPLE WINS : %d / %d ; DRAWS : %d' % (nwins, swins, draws))

            nmcts = MCTS(self.game, self.nnet, self.args)

            print('PITTING AGAINST PREVIOUS VERSION')
            arena = Arena(lambda x: np.argmax(pmcts.getActionProb(x, temp=0, useAlt=True)),
                          lambda x: np.argmax(nmcts.getActionProb(x, temp=0, useAlt=True)), self.game)
            pwins, nwins, draws = arena.playGames(self.args["arenaCompare"])

            print('NEW/PREV WINS : %d / %d ; DRAWS : %d' % (nwins, pwins, draws))
            if pwins+nwins == 0 or float(nwins)/(pwins+nwins) < self.args["updateThreshold"]:
                print('REJECTING NEW MODEL')
                self.nnet.load_checkpoint(folder=self.args["checkpoint"], filename='temp.pth.tar')
            else:
                print('ACCEPTING NEW MODEL')
                self.nnet.save_checkpoint(folder=self.args["checkpoint"], filename=self.getCheckpointFile(i))
                self.nnet.save_checkpoint(folder=self.args["checkpoint"], filename='best.pth.tar')
                self.nnet.save_checkpoint(folder=self.args["checkpoint"], filename='temp.pth.tar')

    def getCheckpointFile(self, iteration):
        return 'checkpoint_' + str(iteration) + '.pth.tar'

    def saveTrainExamples(self, iteration):
        folder = self.args["checkpoint"]
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = os.path.join(folder, self.getCheckpointFile(iteration)+".examples")
        with open(filename, "wb+") as f:
            Pickler(f).dump(self.trainExamplesHistory)
        f.closed

    def loadTrainExamples(self):
        modelFile = os.path.join(self.args["load_examples_file"][0], self.args["load_examples_file"][1])
        examplesFile = modelFile
        if not os.path.isfile(examplesFile):
            print(examplesFile)
            r = input("File with trainExamples not found. Continue? [y|n]")
            if r != "y":
                sys.exit()
        else:
            print("File with trainExamples found. Read it.")
            with open(examplesFile, "rb") as f:
                self.trainExamplesHistory = Unpickler(f).load()
                print("Loaded", len(self.trainExamplesHistory), "iterations /", sum([len(i) for i in self.trainExamplesHistory]), "frames of examples history.")
                if 'pruneExamplesOnLoadTo' in self.args:
                    self.trainExamplesHistory = self.trainExamplesHistory[-self.args["pruneExamplesOnLoadTo"]:]
                    print("Truncated to", len(self.trainExamplesHistory), "iterations /", sum([len(i) for i in self.trainExamplesHistory]), "frames of examples history.")
            f.closed
            # examples based on the model were already collected (loaded)
            self.skipFirstSelfPlay = True
