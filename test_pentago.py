import unittest

from pentago.PentagoGame import PentagoGame
from pentago.PentagoLogic import Board

import numpy as np

from utils import *

class TestPentago(unittest.TestCase):

    def encoding_test(self, game):
        b = Board(game.n)
        moves = b.get_legal_moves(1)
        for move in moves:
            encoded = game.encodeMove(move)
            move2 = game.decodeMove(encoded)
            self.assertEqual(move, move2)
            self.assertLess(encoded, game.getActionSize())

    def test_action_encoding(self):
        self.encoding_test(PentagoGame(False))
        self.encoding_test(PentagoGame(True))

    def action_size_test(self, game):
        b = Board(game.n)
        moves = b.get_legal_moves(1)
        self.assertEqual(len(moves)+1, game.getActionSize())

    def test_action_size(self):
        self.action_size_test(PentagoGame(False))
        self.action_size_test(PentagoGame(True))

    def reshape_test(self, game):
        nt = game._numTiles()
        tensor = np.zeros((nt, 2, game.n, game.n))

        tile1 = 2
        direction1 = 1
        x1 = 3
        y1 = 4

        tensor[tile1, direction1, x1, y1] = 1
        linear = list(np.reshape(tensor, (-1,)))
        encodedI = linear.index(1)
        pos, rot = game.decodeMove(encodedI)
        x2, y2 = pos
        tile2, direction2 = rot

        self.assertEqual(x1, x2)
        self.assertEqual(y1, y2)
        self.assertEqual(tile1, tile2)
        self.assertEqual(direction1, direction2)

    def reshape_test(self, game):
        nt = game._numTiles()
        tensor = np.zeros((nt, 2, game.n, game.n))

        tile1 = 2
        direction1 = 1
        x1 = 3
        y1 = 4

        tensor[tile1, direction1, x1, y1] = 1
        linear = list(np.reshape(tensor, (-1,)))
        encodedI = linear.index(1)
        pos, rot = game.decodeMove(encodedI)
        x2, y2 = pos
        tile2, direction2 = rot

        self.assertEqual(x1, x2)
        self.assertEqual(y1, y2)
        self.assertEqual(tile1, tile2)
        self.assertEqual(direction1, direction2)


    def test_reshape(self):
        self.reshape_test(PentagoGame(False))
        self.reshape_test(PentagoGame(True))

    def test_reshape2(self):
        game = PentagoGame()
        tensor = np.zeros((2, 2, 2, game.n, game.n))

        tile1 = 2
        tileX = 0
        tileY = 1
        direction1 = 1
        x1 = 3
        y1 = 4

        tensor[tileY, tileX, direction1, x1, y1] = 1
        linear = list(np.reshape(tensor, (-1,)))
        encodedI = linear.index(1)
        pos, rot = game.decodeMove(encodedI)
        x2, y2 = pos
        tile2, direction2 = rot

        self.assertEqual(x1, x2)
        self.assertEqual(y1, y2)
        self.assertEqual(tile1, tile2)
        self.assertEqual(direction1, direction2)

    def test_reshape_and_rot1(self):
        game = PentagoGame()

        boardTensor = np.zeros((game.n, game.n))
        actionTensor = np.zeros((2, 2, 2, game.n, game.n))

        tileXInput = 1
        tileYInput = 0
        directionInput = 1
        xInput = 3
        yInput = 4

        actionTensor[tileYInput, tileXInput, directionInput, xInput, yInput] = 1

        # Rotate Left
        tileExpected = 0
        directionExpected = 1
        xExpected = 4
        yExpected = 2

        _boardOut, actionOut = game.rotateBoard(boardTensor, actionTensor, 3, False)
        linear = list(np.reshape(actionOut, (-1,)))
        encodedI = linear.index(1)
        pos, rot = game.decodeMove(encodedI)
        xOut, yOut = pos
        tileOut, directionOut = rot

        self.assertEqual(tileExpected, tileOut)
        self.assertEqual(directionExpected, directionOut)
        self.assertEqual(xExpected, xOut)
        self.assertEqual(yExpected, yOut)

    def test_reshape_and_rot2(self):
        game = PentagoGame()

        boardTensor = np.zeros((game.n, game.n))
        actionTensor = np.zeros((2, 2, 2, game.n, game.n))

        tileXInput = 1
        tileYInput = 0
        directionInput = 1
        xInput = 3
        yInput = 4

        actionTensor[tileYInput, tileXInput,
                     directionInput, xInput, yInput] = 1

        # Rotate Right and Flip
        tileExpected = 2
        directionExpected = 0
        xExpected = 4
        yExpected = 3

        _boardOut, actionOut = game.rotateBoard(boardTensor, actionTensor, 1, True)
        linear = list(np.reshape(actionOut, (-1,)))
        encodedI = linear.index(1)
        pos, rot = game.decodeMove(encodedI)
        xOut, yOut = pos
        tileOut, directionOut = rot

        print(tileOut, directionOut, xOut, yOut)

        self.assertEqual(tileExpected, tileOut)
        self.assertEqual(directionExpected, directionOut)
        self.assertEqual(xExpected, xOut)
        self.assertEqual(yExpected, yOut)

    # def rotation_test(self, game):
    #     b = Board(game.n)

    #     for x in range(self.n):
    #         for y in range(self.n):
    #             b[x][y] =

    #     moves = b.get_legal_moves(1)
    #     for move in moves:
    #         encoded = game._encodeMove(move)
    #         move2 = game._decodeMove(encoded)
    #         self.assertEqual(move, move2)

    # def test_rotation(self):
    #     self.encoding_test(PentagoGame(False))
    #     self.encoding_test(PentagoGame(True))

if __name__ == '__main__':
    unittest.main()
