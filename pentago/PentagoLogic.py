'''
Author: Eric P. Nichols
Date: Feb 8, 2008.
Board class.
Board data:
  1=white, -1=black, 0=empty
  first dim is column , 2nd is row:
     pieces[1][7] is the square in column 2,
     at the opposite end of the board in row 8.
Squares are stored and manipulated as (x,y) tuples.
x is the column, y is the row.
'''
class Board():

    # list of all 8 directions on the board, as (x,y) offsets
    __directions = [(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1)]

    __rotationDirs = ["L", "R"]

    __xlRotationNames = ['TL', 'T', 'TR', 'L', 'C', 'R', 'BL', 'B', 'BR']
    __normalRotationNames = ['TL', 'TR', 'BL', 'BR']

    __rotateLeft = [
       #( from,   to   )
        ((0, 0), (0, 2)),
        ((1, 0), (0, 1)),
        ((2, 0), (0, 0)),
        ((0, 1), (1, 2)),
        ((1, 1), (1, 1)),
        ((2, 1), (1, 0)),
        ((0, 2), (2, 2)),
        ((1, 2), (2, 1)),
        ((2, 2), (2, 0))
    ]
    __rotateRight = [
       #( from,   to   )
        ((0, 0), (2, 0)),
        ((1, 0), (2, 1)),
        ((2, 0), (2, 2)),
        ((0, 1), (1, 0)),
        ((1, 1), (1, 1)),
        ((2, 1), (1, 2)),
        ((0, 2), (0, 0)),
        ((1, 2), (0, 1)),
        ((2, 2), (0, 2))
    ]

    def __init__(self, n):
        "Set up initial board configuration."

        self.n = n
        # Create the empty board array.
        self.pieces = [None]*self.n
        for i in range(self.n):
            self.pieces[i] = [0]*self.n

        self.xl = (self.n == 9)

        self.rotationDirs = self.__rotationDirs
        if self.xl:
            self.rotationNames = self.__xlRotationNames
        else:
            self.rotationNames = self.__normalRotationNames

        self._allRotations = []
        for i in range(len(self.rotationNames)):
            self._allRotations.append((i, 0))
            self._allRotations.append((i, 1))

    # add [][] indexer syntax to the Board
    def __getitem__(self, index):
        return self.pieces[index]

    def get_legal_moves(self, color):
        """Returns all the legal moves for the given color.
        (1 for white, -1 for black
        """
        moves = []  # stores the legal moves.

        # Get all the squares with pieces of the given color.
        for y in range(self.n):
            for x in range(self.n):
                if self[x][y]==0:
                        for rot in self._allRotations:
                            moves.append(((x, y), rot))
        return moves

    def has_legal_moves(self, color):
        for y in range(self.n):
            for x in range(self.n):
                if self[x][y]==0:
                    return True
        return False

    def has_player_won(self, player):
        for x in range(self.n):
            for y in range(self.n):
                if self[x][y] == player:
                    for cx, cy in self.__directions:
                        if 4*cx + x < 0 or 4*cx + x >= self.n or 4*cy + y < 0 or 4*cy + y >= self.n:
                            # make sure this check of 5 in a row will actually stay in bounds of game board
                            continue
                        allFiveInARow = True
                        for i in range(1, 5):
                            tx = i*cx + x
                            ty = i*cy + y
                            if self[tx][ty] != player:
                                allFiveInARow = False
                                break
                        if allFiveInARow:
                            return True

        return False

    def execute_move(self, move, color):
        """Perform the given move on the board; flips pieces as necessary.
        color gives the color pf the piece to play (1=white,-1=black)
        """

        pos, rot = move
        x, y = pos

        # Place new piece
        self[x][y] = color

        self._rotate(rot)

    def getScoreHeuristic(self, player):
        # + = 0
        # ++ = 2
        # +++ = 8
        # ++++ = 20
        # +++++ = 40
        score = 0
        for x in range(self.n):
            for y in range(self.n):
                thisColor = self[x][y]
                if thisColor != 0:
                    for cx, cy in self.__directions:
                        for i in range(1, 5):
                            tx = i*cx + x
                            ty = i*cy + y
                            if tx < 0 or tx >= self.n or ty < 0 or ty >= self.n:
                                break

                            if self[tx][ty] == thisColor:
                                score += thisColor*i
                            else:
                                break
        return score

    def _rotate(self, rotation):
        tile, direction = rotation

        if self.xl:
            tl = 3
        else:
            tl = 2

        # Make backup of the tile unrotated
        oldTile = [None] * 3
        for x in range(3):
            oldTile[x] = [0]*3

        upLeftY = int(tile/tl) * 3
        upLeftX = (tile % tl) * 3
        for x in range(3):
            for y in range(3):
                oldTile[x][y] = self[upLeftX+x][upLeftY+y]

        # Perform rotation by referencing oldTile
        if direction == 0:
            rotList = self.__rotateLeft
        elif direction == 1:
            rotList = self.__rotateRight

        for (frm, to) in rotList:
            frmX, frmY = frm
            toX, toY = to
            self[upLeftX+toX][upLeftY+toY] = oldTile[frmX][frmY]
