import numpy as np
import readline

from .PentagoLogic import Board

class RandomPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        a = np.random.randint(self.game.getActionSize())
        valids = self.game.getValidMoves(board, 1)
        while valids[a]!=1:
            a = np.random.randint(self.game.getActionSize())
        return a


class HumanPentagoPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        # display(board)
        valid = self.game.getValidMoves(board, 1)
        boardObj = Board(self.game.n)

        # print("Move: ", end="")
        while True:
            input_move = input("Move: ")
            input_a = input_move.upper().split(" ")
            if len(input_a) == 4:
                try:
                    x = int(input_a[0])
                    y = int(input_a[1])
                except ValueError:
                    print('Invalid integer')
                    continue
                if not ((0 <= x) and (x < self.game.n) and (0 <= y) and (y < self.game.n)):
                    print("Given coordinates are not a valid board position")
                    continue

                tile_s = input_a[2]
                try:
                    tile = boardObj.rotationNames.index(tile_s)
                except ValueError:
                    print("Invalid tile name", tile_s)
                    print("Choose one of", boardObj.rotationNames)
                    continue

                direction_s = input_a[3]
                try:
                    direction = boardObj.rotationDirs.index(direction_s)
                except ValueError:
                    print("Invalid direction", direction_s)
                    print("Choose one of", boardObj.rotationDirs)
                    continue

                a = self.game.encodeMove(((x, y), (tile, direction)))

                if valid[a]:
                    break
                else:
                    print('Invalid move, this space is taken.')

            else:
                print('Invalid move')
                print("Use the form \"[x] [y] [square] [rotation]\", e.g. \"0 2 TL r\" (TL for top-left, r for right)")
                if self.game.xl:
                    print("Use \"C\" for the center tile.")
        return a

class GreedyPentagoPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        import random

        valids = self.game.getValidMoves(board, 1)
        candidates = []
        for a in range(self.game.getActionSize()):
            if valids[a] == 0:
                continue
            nextBoard, _ = self.game.getNextState(board, 1, a)
            score = self.game.getScore(nextBoard, 1)
            candidates += [(-score, a)]
        candidates.sort()

        bestScore = candidates[0][0]
        tiedCandidates = []
        for negScore, a in candidates:
            if negScore > bestScore:
                break
            else:
                tiedCandidates.append(a)

        return random.choice(tiedCandidates)

# This is too expensive :(
class SimpleLookAheadPentagoPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        import random
        greedy = GreedyPentagoPlayer(self.game)

        valids = self.game.getValidMoves(board, 1)
        candidates = []
        for a in range(self.game.getActionSize()):
            if valids[a] == 0:
                continue
            next1Board, _ = self.game.getNextState(board, 1, a)
            next2Board, _ = self.game.getNextState(next1Board, -1, greedy.play(-1*next1Board))
            score = self.game.getScore(next2Board, 1)
            candidates += [(-score, a)]
        candidates.sort()

        bestScore = candidates[0][0]
        tiedCandidates = []
        for negScore, a in candidates:
            if negScore > bestScore:
                break
            else:
                tiedCandidates.append(a)

        return random.choice(tiedCandidates)

def greedyPolicyMaker(game, board, temp=0):
    valids = game.getValidMoves(board, 1)
    pi = np.zeros(game.getActionSize())
    for a in range(game.getActionSize()):
        if valids[a] == 0:
            continue
        nextBoard, _ = game.getNextState(board, 1, a)
        score = game.getScore(nextBoard, 1)
        pi[a] = np.exp(score)

    sum_pi = np.sum(pi)
    if sum_pi > 0:
        pi /= sum_pi    # renormalize
    else:
        # uh oh, no valid moves left?
        print("Greedy policy had no moves to make, doing workaround.")
        pi = valids
        pi /= np.sum(pi)

    return pi
