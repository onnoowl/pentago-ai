from __future__ import print_function
import sys
sys.path.append('..')
from Game import Game
from .PentagoLogic import Board
import numpy as np

UNDERLINE = "\u0332"
def underline(string):
    return UNDERLINE.join(string)

class PentagoGame(Game):
    square_content = {
        -1: "○",
        +0: "·",
        +1: "●"  # p3: ◁?
    }

    @staticmethod
    def getSquarePiece(piece):
        return PentagoGame.square_content[piece]

    def __init__(self, xl=False):
        self.xl = xl
        if xl:
            self.n = 9
        else:
            self.n = 6

    def getInitBoard(self):
        # return initial board (numpy board)
        b = Board(self.n)
        return np.array(b.pieces)

    def getBoardSize(self):
        # (a,b) tuple
        return (self.n, self.n)

    def getActionSize(self):
        # return number of actions
        if self.xl:
            return 9*9*self._numTiles()*2 + 1
        else:
            return 6*6*self._numTiles()*2 + 1
        # + 1 for an option to indicate that there are no valid moves

    def _numTiles(self):
        if self.xl:
            return 9
        else:
            return 4

    def getNextState(self, board, player, action):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        if action == self.getActionSize() - 1:  # no move possible
            return (board, -player)
        b = Board(self.n)
        b.pieces = np.copy(board)
        move = self.decodeMove(action) # TODO: binary encoding
        b.execute_move(move, player)
        return (b.pieces, -player)

    def getValidMoves(self, board, player):
        # return a fixed size binary vector
        valids = [0]*self.getActionSize()
        b = Board(self.n)
        b.pieces = np.copy(board)
        legalMoves =  b.get_legal_moves(player)
        if len(legalMoves)==0:
            valids[-1]=1
            return np.array(valids)
        for move in legalMoves:
            valids[self.encodeMove(move)]=1
        return np.array(valids)

    # Encodes a move to its binary format (sometimes called an action)
    def encodeMove(self, move):
        pos, rot = move
        x, y = pos
        tile, direction = rot
        posE = self.n*x+y
        rotE = 2*tile+direction

        encoded = self.n*self.n*rotE + posE
        return(encoded)

    # Decodes a move in its binary format (sometimes called an action) to its usual move format, i.e. ((x, y), (tile, dir))
    def decodeMove(self, encoded):
        posE = encoded % (self.n*self.n)
        rotE = int(encoded / (self.n*self.n))

        pos = (int(posE/self.n), posE % self.n)
        rot = (int(rotE/2), rotE % 2)

        return (pos, rot)

    def getGameEnded(self, board, player):
        # return 0 if not ended, 1 if player 1 won, -1 if player 1 lost
        # player = 1
        b = Board(self.n)
        b.pieces = np.copy(board)

        p1Won = b.has_player_won(player)
        p2Won = b.has_player_won(-player)

        if p1Won and p2Won:
            # draw has a very little value
            return 1e-4

        if p1Won:
            return 1
        if p2Won:
            return -1

        if b.has_legal_moves(player):
            return 0

        return 1e-4 #it's a draw, draw has a very little value

    def getCanonicalForm(self, board, player):
        # return state if player==1, else return -state if player==-1
        return player*board

    def getSymmetries(self, board, pi):
        # mirror, rotational
        if self.xl:
            tw = 3 #tile width
        else:
            tw = 2

        pi_board = np.reshape(pi[:-1], (tw, tw, 2, self.n, self.n))  # shape: (tile_y, tile_x, rot direction, x, y)
        l = []

        for numTimes in range(1, 5):
            for flip in [True, False]:
                newB, newPi = self.rotateBoard(board, pi_board, numTimes, flip)
                l += [(newB, list(newPi.ravel()) + [pi[-1]])]
        return l

    @staticmethod
    def rotateBoard(board, pi_board, numTimes, flip):
        newB = np.rot90(board, numTimes)
        newPi = np.rot90(pi_board, numTimes, axes=(3, 4))
        newPi = np.rot90(newPi, numTimes, axes=(1, 0))
        if flip:
            newB = np.fliplr(newB)
            newPi = np.flip(newPi, axis=3)
            newPi = np.flip(newPi, axis=2)
            newPi = np.flip(newPi, axis=1)
        return (newB, newPi)


    def stringRepresentation(self, board):
        return board.tostring()

    def stringRepresentationReadable(self, board):
        board_s = "".join(self.square_content[square] for row in board for square in row)
        return board_s

    def getScore(self, board, player):
        b = Board(self.n)
        b.pieces = np.copy(board)
        return b.getScoreHeuristic(player)

    @staticmethod
    def display(board):
        n = board.shape[0]
        xl = n == 9

        # PRINT TOP ROW
        if xl:
            print("TL    ", end="")
        else:
            print("TL ", end="")

        colNumbers = " "
        for x in range(n):
            colNumbers += str(x) + " "
        colNumbers += " "
        print(underline(colNumbers), end="")

        if xl:
            print(" ", end="")
        print("TR")

        # PRINT INNER BOARD CONTENT
        for y in range(n):
            needsUnderline = y in [2, 5, 8]

            if xl:
                if y == 4:
                    print("L  ", end="")
                else:
                    print("   ", end="")
            print(y, " │", end="")  # print the row #

            for x in range(n):
                if x in [3, 6]:
                    print("│", end="")

                if needsUnderline:
                    print(UNDERLINE, end="")

                piece = board[x][y]    # get the piece to print
                print(PentagoGame.square_content[piece], end="")

                if needsUnderline:
                    print(UNDERLINE, end="")

                if x not in [2, 5, 8]:
                    print(" ", end="")
            print("│", end="")

            if y == 4 and xl:
                print("  R", end="")

            print("")

        # PRINT BOTTOM ROW
        if xl:
            print("BL             B           BR")
        else:
            print("BL               BR")

