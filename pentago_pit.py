import sys
import Arena
from MCTS import MCTS
from pentago.PentagoGame import PentagoGame
from pentago.PentagoPlayers import *
from pentago.tensorflow.NNet import NNetWrapper as NNet
import readline

import numpy as np
from utils import *

"""
use this script to play any two agents against each other, or play manually with
any agent.
"""

mini_pentago = True
aiType = "alpha"
if len(sys.argv) > 1:
    if "--xl" in sys.argv:
        mini_pentago = False
    if "--random" in sys.argv:
        aiType = "random"
    if "--greedy" in sys.argv:
        aiType = "greedy"
    if "--lookahead" in sys.argv:
        aiType = "lookahead"

if mini_pentago:
    g = PentagoGame(False)
else:
    g = PentagoGame(True)

# all players
rp = RandomPlayer(g).play
gp = GreedyPentagoPlayer(g).play
lap = SimpleLookAheadPentagoPlayer(g).play
hp = HumanPentagoPlayer(g).play



# nnet players

# if human_vs_cpu:
#     player2 = hp
# else:
#     n2 = NNet(g)
#     n2.load_checkpoint('./pretrained_models/pentago/pytorch/', '8x8_100checkpoints_best.pth.tar')
#     args2 = dotdict({'numMCTSSims': 50, 'cpuct': 1.0})
#     mcts2 = MCTS(g, n2, args2)
#     n2p = lambda x: np.argmax(mcts2.getActionProb(x, temp=0))

#     player2 = n2p  # Player 2 is neural network if it's cpu vs cpu.

# arena = Arena.Arena(n1p, player2, g, display=PentagoGame.display)

if aiType == "random":
    print("Using a random player.")
    arena = Arena.Arena(hp, rp, g, display=PentagoGame.display)
elif aiType == "greedy":
    print("Using a greedy player.")
    arena = Arena.Arena(hp, gp, g, display=PentagoGame.display)
elif aiType == "lookahead":
    print("Using a simple lookahead player.")
    arena = Arena.Arena(hp, lap, g, display=PentagoGame.display)
elif aiType == "alpha":
    n1 = NNet(g)
    if mini_pentago:
        n1.load_checkpoint('./pretrained_pentago/','best.pth.tar')
    else:
        print("I don't have that model yet.")
        exit(1)
    args1 = {
        'numMCTSSims': 300,
        'retryNumMCTSSims': 125,
        'maxNumMCTSSims': 10_000,
        'cpuct':1,
        'simplePolicy': greedyPolicyMaker,
    } #version in main uses different number of numMTSSims
    # args2 = dotdict({'numMCTSSims': 500, 'cpuct':1, 'simplePolicy': greedyPolicyMaker}) #version in main uses different number of numMTSSims
    mcts1 = MCTS(g, n1, args1)
    n1p = lambda x: np.argmax(mcts1.getActionProb(x, temp=0))

    # mcts2 = MCTS(g, n1, args2)
    # n2p = lambda x: np.argmax(mcts2.getActionProb(x, temp=0))

    print("Using the alphago player.")

    arena = Arena.Arena(hp, n1p, g, display=PentagoGame.display)
else:
    print("Error, unknown aiType", aiType)
    exit(1)

print(arena.playGames(2, verbose=True))
